package com.setcom.robocontroller.ui.screen;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.setcom.robocontroller.App;
import com.setcom.robocontroller.R;
import com.setcom.robocontroller.data.PreferencesStorage;
import com.setcom.robocontroller.data.model.BleDevice;
import com.setcom.robocontroller.di.module.ConnectDeviceActivityModule;
import com.setcom.robocontroller.domain.BleDeviceHelper;
import com.setcom.robocontroller.domain.BleDeviceManager;
import com.setcom.robocontroller.domain.NotifyManager;
import com.setcom.robocontroller.domain.PermissionsHelper;
import com.setcom.robocontroller.domain.model.Info;
import com.setcom.robocontroller.ui.adapter.BaseCellAdapter;
import com.setcom.robocontroller.ui.cell.BleDeviceCell;
import com.setcom.robocontroller.ui.cell.InfoCell;
import com.setcom.robocontroller.ui.custom.DividerItemDecoration;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ConnectDeviceActivity extends AppCompatActivity {

    @BindView(R.id.rv_devices)
    RecyclerView rvDevices;

    @BindView(R.id.pb_load)
    ProgressBar pbLoad;

    @Inject BleDeviceHelper bleDeviceHelper;
    @Inject PreferencesStorage preferencesStorage;
    @Inject PermissionsHelper permissionsHelper;
    @Inject NotifyManager notifyManager;

    private BaseCellAdapter bleDeviceAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connect_device);
        ButterKnife.bind(this);
        App.appComponent()
                .newConnectDeviceActivityComponent(new ConnectDeviceActivityModule(this))
                .inject(this);

        permissionsHelper.requestPermissions();

        rvDevices.addItemDecoration(new DividerItemDecoration(this));

        initAdapter();
    }

    private void initAdapter() {
        bleDeviceAdapter = new BaseCellAdapter<>(this);

        bleDeviceAdapter.registerCell(
                BleDevice.class,
                BleDeviceCell.class,
                new BleDeviceCell.Listener() {
                    @Override
                    public void onCellClicked(BleDevice bleDevice) {
                        deviceClickForSave(bleDevice);
                    }
                }
        );
        bleDeviceAdapter.registerCell(
                Info.class,
                InfoCell.class,
                new InfoCell.Listener() {
                    @Override
                    public void onCellClicked(Info info) {
                        openBTSettings();
                    }
                });

        rvDevices.setAdapter(bleDeviceAdapter);
    }

    private void openBTSettings() {
        final Intent settings = new Intent(Settings.ACTION_BLUETOOTH_SETTINGS);
        startActivity(settings);
        notifyManager.notify(R.string.info_help_text_for_pairing);
    }

    private void deviceClickForSave(BleDevice bleDevice) {
        preferencesStorage.saveBleDevice(bleDevice);
        final Intent data = new Intent(this, MainActivity.class);
        startActivity(data);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!bleDeviceHelper.getBluetoothAdapter().isEnabled()) {
            bleDeviceHelper.requestForEnableBLE();
        } else {
            pbLoad.setVisibility(View.VISIBLE);
            bleDeviceHelper.fetchBleDevices(loadDevicesCallback);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        permissionsHelper.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private BleDeviceManager.LoadDevicesCallback loadDevicesCallback = new BleDeviceManager.LoadDevicesCallback() {
        @Override
        public void onLoaded(List<BleDevice> bleDevices) {
            pbLoad.setVisibility(View.GONE);
            setListDevices(bleDevices);
        }
    };

    private void setListDevices(List<BleDevice> bleDevices) {
        bleDeviceAdapter.clear();
        addInfoItem();
        bleDeviceAdapter.addItems(bleDevices);
    }

    private void addInfoItem() {
        bleDeviceAdapter.addItem(0, new Info(getString(R.string.info_pair_device)));
    }
}
