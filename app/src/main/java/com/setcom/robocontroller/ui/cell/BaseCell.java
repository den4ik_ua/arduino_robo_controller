package com.setcom.robocontroller.ui.cell;

import android.view.View;

import butterknife.ButterKnife;
import io.techery.celladapter.Cell;

/**
 * Description of BaseCell
 *
 * @author Daniil Pavenko <daniil.pavenko@gmail.com>
 * @package com.setcom.robocontroller.ui.cell
 */
public abstract class BaseCell<ITEM, LISTENER extends Cell.Listener<ITEM>> extends Cell<ITEM, LISTENER> {

    public BaseCell(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }
}
