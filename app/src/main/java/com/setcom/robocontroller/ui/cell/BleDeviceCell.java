package com.setcom.robocontroller.ui.cell;

import android.view.View;
import android.widget.TextView;

import com.setcom.robocontroller.R;
import com.setcom.robocontroller.data.model.BleDevice;

import butterknife.BindView;
import io.techery.celladapter.Layout;

/**
 * Description of BleDeviceCell
 *
 * @author Daniil Pavenko <daniil.pavenko@gmail.com>
 * @package com.setcom.robocontroller.ui.cell
 */
@Layout(R.layout.cell_ble_device)
public class BleDeviceCell extends BaseCell<BleDevice, BleDeviceCell.Listener> {

    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_address)
    TextView tvAddress;

    public BleDeviceCell(View view) {
        super(view);
    }

    @Override
    protected void syncUiWithItem() {
        tvName.setText(getItem().name());
        tvAddress.setText(getItem().address());
    }

    public interface Listener extends BaseCell.Listener<BleDevice> {
    }
}
