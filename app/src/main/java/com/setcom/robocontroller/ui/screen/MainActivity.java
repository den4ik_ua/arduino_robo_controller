package com.setcom.robocontroller.ui.screen;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import com.setcom.robocontroller.App;
import com.setcom.robocontroller.R;
import com.setcom.robocontroller.domain.BleDeviceHelper;
import com.setcom.robocontroller.domain.BleDeviceManager;
import com.setcom.robocontroller.ui.custom.VertSeekBar;

import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity implements View.OnTouchListener, SeekBar.OnSeekBarChangeListener {

    @BindView(R.id.tvConnectedStatus)
    TextView tvConnectedStatus;
    @BindView(R.id.tvHumidity)
    TextView tvHumidity;
    @BindView(R.id.tvTemperature)
    TextView tvTemperature;
    @BindView(R.id.sb_speed)
    VertSeekBar sbSpeed;
    @BindView(R.id.btn_go_L)
    Button btnGoL;
    @BindView(R.id.btn_go_R)
    Button btnGoR;
    @BindView(R.id.btn_back_L)
    Button btnBackL;
    @BindView(R.id.btn_back_R)
    Button btnBackR;
    @BindView(R.id.btn_retry_connect)
    Button btnRetryConnect;

    @Inject
    BleDeviceHelper bleDeviceHelper;

    StringBuilder recDataString = new StringBuilder();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        App.appComponent().add().inject(this);

        initListeners();

        connectToDevice();

        startCheckerConnection();
    }

    private void startCheckerConnection() {
        Timer timer = new Timer(true);
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                checkConnectionStatus();
            }
        }, 0, 1000);
    }

    private void connectToDevice() {
        bleDeviceHelper.connectToDevice();
        bleDeviceHelper.createSocket(new BleDeviceManager.ConnectedSocketCallback() {
            @Override
            public void onConnect() {
                bleDeviceHelper.setReceiverData(bleDataCallback);

                checkConnectionStatus();
            }

            @Override
            public void onFail() {
                checkConnectionStatus();
            }
        });
    }

    private void initListeners() {
        btnGoL.setOnTouchListener(this);
        btnBackL.setOnTouchListener(this);
        btnGoR.setOnTouchListener(this);
        btnBackR.setOnTouchListener(this);
        sbSpeed.setOnSeekBarChangeListener(this);
    }

    @OnClick(R.id.btn_retry_connect)
    public void onRetryConnect() {
        connectToDevice();
    }

    private void checkConnectionStatus() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tvConnectedStatus.setText(
                        bleDeviceHelper.isConnectedBLE() ? R.string.connected : R.string.disconnect
                );
            }
        });
    }

    private BleDeviceManager.BleDataCallback bleDataCallback = new BleDeviceManager.BleDataCallback() {
        @Override
        public void receiveData(byte[] data, int numBytes) {
            //data = ByteUtils.trim(data);
            recDataString.append(new String(data, 0, numBytes));
            int endOfLineIndex = recDataString.indexOf("~");
            int separateOfLineIndex = recDataString.indexOf("|");
            if (endOfLineIndex > 0 && separateOfLineIndex > 0) {
                String dataInPrint = recDataString.substring(0, endOfLineIndex);

                if (recDataString.charAt(0) == '#') {
                    setDetectorData(
                            recDataString.substring(1, separateOfLineIndex),
                            recDataString.substring(separateOfLineIndex + 1, endOfLineIndex)
                    );
                }
                recDataString.delete(0, recDataString.length());
                dataInPrint = " ";
            }
        }
    };

    private void setDetectorData(final String humidity, final String temperature) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tvHumidity.setText(String.format(Locale.US, "%s %%", humidity));
                tvTemperature.setText(temperature + " " + (char) 0x00B0 + "C");
            }
        });
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
            bleDeviceHelper.sendCommand(BleDeviceManager.Command.START);
        } else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
            bleDeviceHelper.sendCommand(BleDeviceManager.Command.STOP);
            return true;
        }

        switch (view.getId()) {
            case R.id.btn_go_L:
                bleDeviceHelper.sendCommand(BleDeviceManager.Command.GO_L);
                break;
            case R.id.btn_back_L:
                bleDeviceHelper.sendCommand(BleDeviceManager.Command.BACK_L);
                break;
            case R.id.btn_go_R:
                bleDeviceHelper.sendCommand(BleDeviceManager.Command.GO_R);
                break;
            case R.id.btn_back_R:
                bleDeviceHelper.sendCommand(BleDeviceManager.Command.BACK_R);
                break;
        }

        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        bleDeviceHelper.disconnectDevice();
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        bleDeviceHelper.sendCommand(BleDeviceManager.Command.SPEED);
        bleDeviceHelper.sendCommand(seekBar.getProgress());
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
    }
}
