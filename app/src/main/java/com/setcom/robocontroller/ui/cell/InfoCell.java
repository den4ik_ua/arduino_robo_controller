package com.setcom.robocontroller.ui.cell;

import android.view.View;
import android.widget.TextView;

import com.setcom.robocontroller.R;
import com.setcom.robocontroller.domain.model.Info;

import butterknife.BindView;
import io.techery.celladapter.Layout;

/**
 * Description of InfoCell
 *
 * @author Daniil Pavenko <daniil.pavenko@gmail.com>
 * @package com.setcom.robocontroller.ui.cell
 */
@Layout(R.layout.cell_info)
public class InfoCell extends BaseCell<Info, InfoCell.Listener> {

    @BindView(R.id.tv_text) TextView tvText;

    public InfoCell(View view) {
        super(view);
    }

    @Override
    protected void syncUiWithItem() {
        tvText.setText(getItem().text());
    }

    public interface Listener extends BaseCell.Listener<Info> {
    }
}
