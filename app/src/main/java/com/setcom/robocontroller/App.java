package com.setcom.robocontroller;

import android.app.Application;

import com.setcom.robocontroller.di.component.AppComponent;
import com.setcom.robocontroller.di.component.DaggerAppComponent;
import com.setcom.robocontroller.di.module.AppModule;

/**
 * Description of App
 *
 * @author Daniil Pavenko <daniil.pavenko@gmail.com>
 * @package com.setcom.robocontroller
 */
public class App extends Application {

    private static AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }

    public static AppComponent appComponent() {
        return appComponent;
    }
}
