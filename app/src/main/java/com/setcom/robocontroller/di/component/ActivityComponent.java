package com.setcom.robocontroller.di.component;

import com.setcom.robocontroller.ui.screen.MainActivity;

import dagger.Subcomponent;

/**
 * Description of ActivityComponent
 *
 * @author Daniil Pavenko <daniil.pavenko@gmail.com>
 * @package com.setcom.robocontroller.di.component
 */
@Subcomponent
public interface ActivityComponent {

    void inject(MainActivity mainActivity);
}
