package com.setcom.robocontroller.di.component;

import com.setcom.robocontroller.di.module.ConnectDeviceActivityModule;
import com.setcom.robocontroller.di.scope.ConnectDeviceActivityScope;
import com.setcom.robocontroller.ui.screen.ConnectDeviceActivity;

import dagger.Subcomponent;

/**
 * Description of ConnectDeviceActivityComponent
 *
 * @author Daniil Pavenko <daniil.pavenko@gmail.com>
 * @package com.setcom.robocontroller.di.component
 */
@ConnectDeviceActivityScope
@Subcomponent(modules = {
        ConnectDeviceActivityModule.class
})
public interface ConnectDeviceActivityComponent {

    void inject(ConnectDeviceActivity connectDeviceActivity);
}
