package com.setcom.robocontroller.di.component;

import com.setcom.robocontroller.di.module.AppModule;
import com.setcom.robocontroller.di.module.ConnectDeviceActivityModule;
import com.setcom.robocontroller.di.module.ServiceModule;
import com.setcom.robocontroller.di.module.StorageModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Description of AppComponent
 *
 * @author Daniil Pavenko <daniil.pavenko@gmail.com>
 * @package com.setcom.robocontroller.di
 */
@Singleton
@Component(modules = {
        AppModule.class,
        StorageModule.class,
        ServiceModule.class
})
public interface AppComponent {

    ActivityComponent add();

    ConnectDeviceActivityComponent newConnectDeviceActivityComponent(ConnectDeviceActivityModule connectDeviceActivityModule);
}
