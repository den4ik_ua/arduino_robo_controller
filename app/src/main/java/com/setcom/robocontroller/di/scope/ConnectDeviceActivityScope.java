package com.setcom.robocontroller.di.scope;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Description of ConnectDeviceActivityScope
 *
 * @author Daniil Pavenko <daniil.pavenko@gmail.com>
 * @package com.setcom.robocontroller.di.scope
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface ConnectDeviceActivityScope {
}
