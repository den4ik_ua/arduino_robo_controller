package com.setcom.robocontroller.di.module;

import com.setcom.robocontroller.di.scope.ConnectDeviceActivityScope;
import com.setcom.robocontroller.domain.PermissionsHelper;
import com.setcom.robocontroller.domain.PermissionsManager;
import com.setcom.robocontroller.ui.screen.ConnectDeviceActivity;

import dagger.Module;
import dagger.Provides;

/**
 * Description of ConnectDeviceActivityModule
 *
 * @author Daniil Pavenko <daniil.pavenko@gmail.com>
 * @package com.setcom.robocontroller.di.module
 */
@Module
public class ConnectDeviceActivityModule {

    private final ConnectDeviceActivity activity;

    public ConnectDeviceActivityModule(ConnectDeviceActivity activity) {
        this.activity = activity;
    }

    @Provides
    @ConnectDeviceActivityScope
    PermissionsHelper providePermissionsManager() {
        return new PermissionsManager(activity);
    }
}
