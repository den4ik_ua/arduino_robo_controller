package com.setcom.robocontroller.di.module;

import android.app.Application;
import android.content.Context;

import com.setcom.robocontroller.domain.NotifyManager;
import com.setcom.robocontroller.domain.ToastNotifyManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Description of AppModule
 *
 * @author Daniil Pavenko <daniil.pavenko@gmail.com>
 * @package com.setcom.robocontroller.di
 */
@Module
public class AppModule {

    private final Application application;

    public AppModule(Application application) {
        this.application = application;
    }

    @Singleton
    @Provides
    Context provideApplicationContext() {
        return application.getApplicationContext();
    }

    @Singleton
    @Provides
    NotifyManager provideNotifyManager(Context context) {
        return new ToastNotifyManager(context);
    }
}
