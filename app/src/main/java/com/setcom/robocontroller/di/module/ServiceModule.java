package com.setcom.robocontroller.di.module;

import android.content.Context;

import com.setcom.robocontroller.data.PreferencesStorage;
import com.setcom.robocontroller.domain.BleDeviceHelper;
import com.setcom.robocontroller.domain.BleDeviceManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Description of ServiceModule
 *
 * @author Daniil Pavenko <daniil.pavenko@gmail.com>
 * @package com.setcom.robocontroller.di.module
 */
@Module(includes = {
        StorageModule.class
})
public class ServiceModule {

    @Singleton
    @Provides
    BleDeviceHelper provideBleDeviceManager(Context context, PreferencesStorage preferencesStorage) {
        return new BleDeviceManager(context, preferencesStorage);
    }
}
