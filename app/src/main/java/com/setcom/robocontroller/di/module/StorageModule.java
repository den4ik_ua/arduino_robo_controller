package com.setcom.robocontroller.di.module;

import android.content.Context;

import com.setcom.robocontroller.data.PreferencesStorageImpl;
import com.setcom.robocontroller.data.PreferencesStorage;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Description of StorageModule
 *
 * @author Daniil Pavenko <daniil.pavenko@gmail.com>
 * @package com.setcom.robocontroller.di.module
 */
@Module
public class StorageModule {

    @Singleton
    @Provides
    PreferencesStorage providePreferencesStorage(Context context) {
        return new PreferencesStorageImpl(context);
    }
}
