package com.setcom.robocontroller.domain;

import android.support.annotation.StringRes;

/**
 * Description of NotifyManager
 *
 * @author Daniil Pavenko <daniil.pavenko@gmail.com>
 * @package com.setcom.robocontroller.domain
 */
public interface NotifyManager {

    void notify(@StringRes int textResId);
}
