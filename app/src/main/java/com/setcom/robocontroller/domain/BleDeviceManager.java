package com.setcom.robocontroller.domain;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;

import com.setcom.robocontroller.data.PreferencesStorage;
import com.setcom.robocontroller.data.model.BleDevice;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static android.content.ContentValues.TAG;

/**
 * Description of BleDeviceManager
 *
 * @author Daniil Pavenko <daniil.pavenko@gmail.com>
 * @package com.setcom.robocontroller.manager
 */
public class BleDeviceManager implements BleDeviceHelper {

    public static final UUID APP_UUID = UUID.randomUUID();

    public static final int REQUEST_ENABLE_BT = 12;

    public static final String BLE_NAME = "robo";

    private Context context;

    private BluetoothAdapter bluetoothAdapter;

    private PreferencesStorage preferencesStorage;

    public BleDeviceManager(Context context, PreferencesStorage preferencesStorage) {
        this.context = context;
        this.preferencesStorage = preferencesStorage;
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    }

    @Override
    public BluetoothAdapter getBluetoothAdapter() {
        return bluetoothAdapter;
    }

    @Override
    public void fetchBleDevices(final LoadDevicesCallback loadDevicesCallback) {
        new AsyncTask<Void, Void, List<BleDevice>>() {
            @Override
            protected List<BleDevice> doInBackground(Void... voids) {
                final Set<BluetoothDevice> pairedDevices = bluetoothAdapter.getBondedDevices();

                final List<BleDevice> bleDevices = new ArrayList<>();

                if (pairedDevices.size() > 0) {
                    for (BluetoothDevice device : pairedDevices) {
                        String deviceName = device.getName();
                        String deviceHardwareAddress = device.getAddress(); // MAC address
                        UUID uuid = null;
                        if (device.getUuids() != null && device.getUuids().length > 0) {
                            uuid = device.getUuids()[0].getUuid();
                        }
                        bleDevices.add(new BleDevice(deviceName, deviceHardwareAddress, uuid));
                    }
                }
                return bleDevices;
            }

            @Override
            protected void onPostExecute(List<BleDevice> bleDevices) {
                super.onPostExecute(bleDevices);
                loadDevicesCallback.onLoaded(bleDevices);
            }
        }.execute();
    }

    @Override
    public void requestForEnableBLE() {
        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        ((Activity)context).startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
    }

    private volatile BluetoothSocket socketCommand = null;
    private volatile BluetoothDevice mmDevice = null;
    private volatile InputStream mmInStream;
    private volatile OutputStream mmOutStream;
    private byte[] mmBuffer;

    @Override
    public void connectToDevice() {
        BluetoothSocket tmp = null;
        try {
            BleDevice device = preferencesStorage.getBleDevice();
            // MY_UUID is the app's UUID string, also used by the client code.
            mmDevice = bluetoothAdapter.getRemoteDevice(device.address());

            tmp = mmDevice.createInsecureRfcommSocketToServiceRecord(device.uuid());
        } catch (IOException e) {
            e.printStackTrace();
        }
        socketCommand = tmp;
    }

    @Override
    public void createSocket(@NonNull final ConnectedSocketCallback connectedSocketCallback) {
        if(socketCommand == null) return;
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    try {
                        bluetoothAdapter.cancelDiscovery();
                        if(socketCommand != null) socketCommand.connect();
                    } catch (IOException e) {
                        Log.e(TAG, "Socket's accept() method failed", e);
                        connectedSocketCallback.onFail();
                        break;
                    }

                    if (socketCommand != null) {
                        // A connection was accepted. Perform work associated with
                        // the connection in a separate thread.
                        InputStream tmpIn = null;
                        OutputStream tmpOut = null;

                        // Get the input and output streams; using temp objects because
                        // member streams are final.
                        try {
                            tmpIn = socketCommand.getInputStream();
                        } catch (IOException e) {
                            Log.e(TAG, "Error occurred when creating input stream", e);
                        }
                        try {
                            tmpOut = socketCommand.getOutputStream();
                        } catch (IOException e) {
                            Log.e(TAG, "Error occurred when creating output stream", e);
                        }

                        mmInStream = tmpIn;
                        mmOutStream = tmpOut;

                        connectedSocketCallback.onConnect();
                        break;
                    }
                }
            }
        }).start();
    }

    @Override
    public void setReceiverData(final BleDataCallback bleDataCallback) {
        if(socketCommand == null || mmInStream == null) return;
        new Thread(new Runnable() {
            @Override
            public void run() {
                mmBuffer = new byte[256];
                int numBytes;

                // Keep listening to the InputStream until an exception occurs.
                while (true) try {
                    // Read from the InputStream.
                    numBytes = mmInStream.read(mmBuffer);
                    // Send the obtained bytes to the UI activity.
                    bleDataCallback.receiveData(mmBuffer, numBytes);
                } catch (IOException e) {
                    Log.d(TAG, "Input stream was disconnected", e);
                    break;
                }
            }
        }).start();
    }

    @Override
    public boolean isConnectedBLE() {
        return socketCommand != null && socketCommand.isConnected() && bluetoothAdapter.isEnabled();
    }

    @Override
    public void disconnectDevice() {
        if (socketCommand != null && socketCommand.isConnected()) {
            try {
                socketCommand.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void sendCommand(int command) {
        if (mmOutStream == null) return;
        try {
            mmOutStream.write(command);
            Log.d(TAG, "Command " + command + " is sent");
        } catch (IOException e) {
            e.printStackTrace();
            Log.e(TAG, "Error sending command");
        }
    }

    public static final class Command {

        public static final int GO_L = 1;
        public static final int GO_R = 2;

        public static final int BACK_L = 3;
        public static final int BACK_R = 4;

        public static final int START = 5;
        public static final int STOP = 6;

        public static final int SPEED = 7;
    }
}
