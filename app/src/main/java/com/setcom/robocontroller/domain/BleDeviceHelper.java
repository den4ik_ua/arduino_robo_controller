package com.setcom.robocontroller.domain;

import android.bluetooth.BluetoothAdapter;
import android.support.annotation.NonNull;

import com.setcom.robocontroller.data.model.BleDevice;

import java.util.List;

/**
 * Description of BleDeviceManager
 *
 * @author Daniil Pavenko <daniil.pavenko@gmail.com>
 * @package com.setcom.robocontroller.manager
 */
public interface BleDeviceHelper {

    BluetoothAdapter getBluetoothAdapter();

    void fetchBleDevices(final LoadDevicesCallback loadDevicesCallback);

    void requestForEnableBLE();

    void connectToDevice();

    void createSocket(@NonNull final ConnectedSocketCallback connectedSocketCallback);

    void setReceiverData(final BleDataCallback bleDataCallback);

    boolean isConnectedBLE();

    void disconnectDevice();

    void sendCommand(int command);

    interface BleDataCallback {

        void receiveData(byte[] data, int numBytes);
    }

    interface LoadDevicesCallback {

        void onLoaded(List<BleDevice> bleDevices);
    }

    interface ConnectedSocketCallback {

        void onConnect();

        void onFail();
    }
}
