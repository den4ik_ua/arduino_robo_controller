package com.setcom.robocontroller.domain;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

/**
 * Description of PermissionsManager
 *
 * @author Daniil Pavenko <daniil.pavenko@gmail.com>
 * @package com.setcom.robocontroller.manager
 */
public class PermissionsManager implements PermissionsHelper{

    public static final int REQUEST_PERMISSIONS = 123;

    private Activity activity;

    public PermissionsManager(Activity activity) {
        this.activity = activity;
    }

    @Override
    public boolean requestPermissions() {
        if (checkBluetoothAdminPermissionGranted()
                && checkAdminPermissionGranted()) {
            return true;
        } else {
            ActivityCompat.requestPermissions(activity,
                    new String[]{
                            Manifest.permission.BLUETOOTH,
                            Manifest.permission.BLUETOOTH_ADMIN},
                    REQUEST_PERMISSIONS);
            return false;
        }
    }

    @Override
    public boolean onRequestPermissionsResult(int requestCode,
                                              String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSIONS: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    System.out.println("Permissions granted!");
                    return true;
                } else {
                    System.out.println("Permissions blocked!");
                    activity.finish();
                    return false;
                }
            }
        }
        return false;
    }

    private boolean checkAdminPermissionGranted() {
        return ContextCompat.checkSelfPermission(activity,
                Manifest.permission.BLUETOOTH)
                == PackageManager.PERMISSION_GRANTED;
    }

    private boolean checkBluetoothAdminPermissionGranted() {
        return ContextCompat.checkSelfPermission(activity,
                Manifest.permission.BLUETOOTH_ADMIN)
                == PackageManager.PERMISSION_GRANTED;
    }
}
