package com.setcom.robocontroller.domain.model;

/**
 * Description of Info
 *
 * @author Daniil Pavenko <daniil.pavenko@gmail.com>
 * @package com.setcom.robocontroller.domain.model
 */
public class Info {

    private String text;

    public Info(String text) {
        this.text = text;
    }

    public String text() {
        return text;
    }
}
