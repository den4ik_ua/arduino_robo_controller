package com.setcom.robocontroller.domain;

/**
 * Description of PermissionsHelper
 *
 * @author Daniil Pavenko <daniil.pavenko@gmail.com>
 * @package com.setcom.robocontroller.domain
 */
public interface PermissionsHelper {

    boolean requestPermissions();

    boolean onRequestPermissionsResult(int requestCode,
                                       String permissions[], int[] grantResults);
}
