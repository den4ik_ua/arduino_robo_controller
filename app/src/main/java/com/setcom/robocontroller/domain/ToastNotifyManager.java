package com.setcom.robocontroller.domain;

import android.content.Context;
import android.support.annotation.StringRes;
import android.view.Gravity;
import android.widget.Toast;

/**
 * Description of ToastNotifyManager
 *
 * @author Daniil Pavenko <daniil.pavenko@gmail.com>
 * @package com.setcom.robocontroller.domain
 */
public class ToastNotifyManager implements NotifyManager {

    private Context context;

    public ToastNotifyManager(Context context) {
        this.context = context;
    }

    @Override
    public void notify(@StringRes int textResId) {
        Toast toast = Toast.makeText(context, textResId, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }
}
