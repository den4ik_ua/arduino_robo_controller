package com.setcom.robocontroller.data.model;

import java.util.UUID;

/**
 * Description of BleDevice
 *
 * @author Daniil Pavenko <daniil.pavenko@gmail.com>
 * @package com.setcom.robocontroller.data.model
 */
public class BleDevice {

    private String name;
    private String address;
    private UUID uuid;

    public BleDevice(String name, String address, UUID uuid) {
        this.name = name;
        this.address = address;
        this.uuid = uuid;
    }

    public String name() {
        return name;
    }

    public String address() {
        return address;
    }

    public UUID uuid() {
        return uuid;
    }
}
