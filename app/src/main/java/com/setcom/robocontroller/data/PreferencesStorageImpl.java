package com.setcom.robocontroller.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.setcom.robocontroller.data.model.BleDevice;

import java.util.UUID;

/**
 * Description of PreferencesStorageImpl
 *
 * @author Daniil Pavenko <daniil.pavenko@gmail.com>
 * @package com.setcom.robocontroller.data
 */
public class PreferencesStorageImpl implements PreferencesStorage {

    private static final String NAME = "robo_controller";
    private static final String DEVICE_NAME = "DEVICE_NAME";
    private static final String DEVICE_ADDRESS = "DEVICE_ADDRESS";
    private static final String DEVICE_UUID = "DEVICE_UUID";

    private SharedPreferences prefs;

    public PreferencesStorageImpl(Context context) {
        prefs = context.getSharedPreferences(NAME, Context.MODE_PRIVATE);
    }

    @Override
    public void saveBleDevice(BleDevice bleDevice) {
        prefs.edit()
                .putString(DEVICE_NAME, bleDevice.name())
                .putString(DEVICE_ADDRESS, bleDevice.address())
                .putString(DEVICE_UUID, bleDevice.uuid() != null ? bleDevice.uuid().toString() : "")
                .apply();
    }

    @Override
    public BleDevice getBleDevice() {
        if (prefs.contains(DEVICE_NAME)) {
            final String uuid = prefs.getString(DEVICE_UUID, null);
            return new BleDevice(
                    prefs.getString(DEVICE_NAME, null),
                    prefs.getString(DEVICE_ADDRESS, null),
                    !TextUtils.isEmpty(uuid) ? UUID.fromString(prefs.getString(DEVICE_UUID, null)) : UUID.randomUUID()
            );
        } else {
            return null;
        }
    }
}
