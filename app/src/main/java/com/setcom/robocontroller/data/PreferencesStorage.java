package com.setcom.robocontroller.data;

import com.setcom.robocontroller.data.model.BleDevice;

/**
 * Description of PreferencesStorage
 *
 * @author Daniil Pavenko <daniil.pavenko@gmail.com>
 * @package com.setcom.robocontroller.manager
 */
public interface PreferencesStorage {

    void saveBleDevice(BleDevice bleDevice);

    BleDevice getBleDevice();
}
